Since its inception, cryptocurrency has generated both negative and positive reactions. What is certain is that the universal adoption of cryptocurrency would bring about significant changes. However, as long as cryptocurrency remains a proposal and a potential solution, it is difficult to fully assess and prove the effects of its adoption.

To provide a starting point, this document gathers a series of questions that can be examined within the context of a society where fiat currency has been replaced by cryptocurrency. Reasoning within the context of communitycoins, we want to consider a restricted set of global cryptocurrencies combined with a second layer of non-competing "sovereign" communitycoins. "Other cryptocurrencies" would be considered as not-allocatable to a region and probably competing with global cryptocurrencies for global dominance.

## Questions regarding personal freedom

- Laws and regulations: Laws can prohibit or restrict certain behaviors or actions. In what ways do you see cryptocurrency as a solution to restrictive legislation that limits personal freedom?
- Constitutional limitations: In some countries, constitutional limitations restrict freedom of speech, religion, assembly, association, and other fundamental rights. These limitations may be imposed to protect public order, national security, or the rights and freedoms of others.
- Censorship: Governments can impose restrictions on media and communication channels, such as the internet, to control or limit the dissemination of certain information. This can restrict freedom of speech and access to information. Where can communitycoins guarantee access that is otherwise inaccessible?
- Surveillance: In some countries, governments may establish extensive surveillance programs to monitor citizens, control their communication, or monitor their behavior. This can limit individual privacy and undermine feelings of freedom. How does communitycoins enhance people's privacy?
- Mobility restrictions: Governments can impose travel restrictions, such as visa requirements, border controls, immigration policies, and access restrictions to certain areas, which can limit the freedom of movement of individuals. How can communitycoins improve people's mobility?
- Restrictions on association and assembly: In some countries, governments may require permits or permissions for public gatherings, demonstrations, or protests. This can limit people's freedom to assemble and voice their opinions. How can we utilize communitycoins to ensure everyone has a voice?
- Economic restrictions: Governments can impose economic limitations, such as trade sanctions, price controls, or property seizures, which can restrict the economic freedom of individuals and businesses. What restrictions currently experienced by people could communitycoins remove?

## Situations involving injustice. 
- Unequal wealth distribution:
The financial system has resulted in significant wealth inequality, with a growing gap between the wealthy elite and the rest of the population. This inequality can lead to social tensions and limited opportunities for those with fewer financial resources. However, cryptocurrency also exhibits substantial inequality, with early adopters holding disproportionately large amounts of coins. Crypto is inaccessible to the "underclass." I don't see a difference in this aspect, but I'm open to arguments.
- Speculation and financial crises:
The current financial system sometimes encourages speculative practices that lead to volatility and financial crises. Such crises often have a devastating impact on the broader economy and primarily affect those least able to protect themselves, such as the working class and the middle class. Cryptocurrency, one could argue, is inherently speculative. It lacks underlying value, and its price is based on the well-known "greater fool principle."
- Power of financial institutions:
Large financial institutions, such as banks and investment firms, often wield significant power and influence within the financial system. This can result in decision-making that serves their interests at the expense of the general welfare. Moreover, some financial institutions have engaged in reckless behavior and risky investments that can be harmful to the broader economy. Cryptocurrency could potentially score better on this point if it manages to gain widespread acceptance as a financial medium.
- Lack of transparency:
The financial system can be complex and opaque, making it difficult for ordinary people to understand how it operates and what consequences it has on their lives. This lack of transparency can lead to mistrust and frustration among the public. However, the complexity of cryptocurrency is such that 90% of people claim to have little to no understanding of it. Crypto scores poorly in this regard. For most people, cryptocurrency is inaccessible and only suitable for a small, technologically adept elite.
- Negative environmental impact:
The current financial system often has a negative impact on the environment, promoting economic growth at the expense of natural resources and contributing to climate change. The costs of these impacts are often inadequately internalized, shifting the burdens onto future generations and vulnerable communities. The energy consumption of coins like Bitcoin is such that it has already been banned in some places for that reason alone.

## Situations at the societal and political level

## Questions regarding communitycoins as opposed to other coins/tokens/icos

- What should be the answer to incoming (push) requests to join communitycoins and how can we manage them? Can we categorize such requests, award them, provide backlinks or even arrange or stimulate pull requests
