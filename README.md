# CommunityCoins, a Collaborative Think Tank

CommunityCoins aims to establish itself as a dynamic think tank where teams come together to engage in thought-provoking discussions and generate innovative answers. Simultaneously, they are open to receiving questions from the communities they serve and actively respond to them. The primary objective of this project is to provide structure to these inquiries and responses while fostering a culture of continuous questioning and external input.

Next key specifications are presented. By anserwing these CommunityCoins will effectively harness the collective intelligence of its participants, serving as a robust platform for knowledge sharing, innovation, and impactful contributions to a wide range of topics.

## Key Specifications of the Collaborative Think Tank project:

- Streamlined Question and Answer Process: CommunityCoins will streamline the process of posing questions and finding answers by creating a systematic framework. This framework will ensure that inquiries are well-organized, enabling efficient collaboration and knowledge sharing among the participants.

- Comprehensive Reference Repository: The project will serve as a valuable reference repository, accumulating a wealth of collective wisdom and insights. By categorizing and cataloging the questions and answers, CommunityCoins will provide a reliable resource that participants and community members can refer to for relevant information.

- Avoidance of Redundancy: CommunityCoins seeks to prevent the reinvention of the wheel by encouraging participants to explore existing discussions and answers. By implementing effective search and tagging mechanisms, duplicate questions can be minimized, and participants can build upon previous insights and solutions.

- Consensus and Disagreement Monitoring: The project will closely monitor the consensus and disagreements that emerge within the discussions. This monitoring will shed light on areas of agreement, enabling the identification of common ground, while also highlighting divergent perspectives that can stimulate further exploration and debate.

- Synergies with Social Media and Other Projects: CommunityCoins will act as a vital feeder for social media posts, other projects, and the identification of functional requirements. Valuable insights and engaging discussions generated within the think tank can be shared across social media platforms, used as inspiration for other projects, and inform the development of functional requirements for various initiatives.

- Encouragement of Participation: CommunityCoins will actively promote and incite participation from a diverse range of individuals, both within and outside the think tank. By cultivating an inclusive and collaborative environment, the project aims to attract individuals from different backgrounds, perspectives, and areas of expertise, fostering a rich ecosystem of ideas and insights.

## Collaboration, interaction, Monitoring

To establish collaboration public interaction is key. Some considerations:
- At this stage, without any questions, pre-indexing them makes no sence. However, we can start with a [bootstrap](https://gitlab.com/c4319/questions-and-answers/-/blob/main/Bootstrap.md) that contains sets of questions to trigger kick-off. 

Before chosing an optimal or fixed format to present questions and answers we need some considerations. The total amount of questions can be enormous and the amount of answers even more. Therefor only a limited set can be visible at one time with added functions such as scrolling, paging, expansion
- Full text search functions
- Cathegories: 
    - pulication date
    - Normal Reading time / based on character count
    - Reading time
    - % read = Reading time / Normal reading time
    - Hits
    - public rating
    - team rating:
        - Proficiency
        - Relevance
        - Depth of Analysis
        - Clarity and Coherence
        - Supporting Evidence
        - Originality and Creativity
        - Structure and Format
        - average team rating
